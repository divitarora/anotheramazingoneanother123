package com.maf.AnotherAmazingOneAnother;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnotherAmazingOneAnother123Application {

	public static void main(String[] args) {
		SpringApplication.run(AnotherAmazingOneAnother123Application.class, args);
	}

}
